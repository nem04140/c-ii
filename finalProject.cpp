#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;


void hero1()
{
    //Opening of stream, declaration of vector
	ifstream inFile;
	vector<string> hero1;
	string getstring;

	inFile.open("Hero1.txt");

    //Loop for getting each line of string from the text file
	 while (getline(inFile, getstring))
	{
		// Line contains string of length > 0 then save it in vector
		if(getstring.size() > 0)
			hero1.push_back(getstring);//Enters text into the vector of hero1
	}

	srand ( time(NULL) ); //initialize the random seed

    int RandIndex = rand() % 219;//Random number up the specified number
    cout << hero1[RandIndex];//Random number that was chosen is used to chose element


    //Testing for proper size and placement within vector
    //cout << hero1.size() << endl;
    //cout << hero1[0] << endl;
	inFile.close();
}

void hero2()
{
    //Opening of stream, declaration of vector
	ifstream inFile;
	vector<string> hero2;
	string getstring;

	inFile.open("Hero2.txt");

	 while (getline(inFile, getstring))
	{
		// Line contains string of length > 0 then save it in vector
		if(getstring.size() > 0)
			hero2.push_back(getstring);
	}

    srand ( time(NULL) ); //initialize the random seed

    int RandIndex = rand() % 238;
    cout << hero2[RandIndex];

    //Testing for proper size and placement within vector
    //cout << hero2.size() << endl;
    //cout << hero2[0] << endl;
	inFile.close();
}

void heroAdj()
{
    //Opening of stream, declaration of vector
	ifstream inFile;
	vector<string> heroAdj;
	string getstring;

	inFile.open("HeroAdj.txt");

	 while (getline(inFile, getstring))
	{
		// Line contains string of length > 0 then save it in vector
		if(getstring.size() > 0)
			heroAdj.push_back(getstring);
	}

	srand ( time(NULL) ); //initialize the random seed

    int RandIndex = rand() % 98;
    cout << heroAdj[RandIndex];

    //Testing for proper size and placement within vector
    //cout << heroAdj.size() << endl;
    //cout << heroAdj[0] << endl;
	inFile.close();
}

void heroVehicle()
{
    //Opening of stream, declaration of vector
	ifstream inFile;
	vector<string> heroVehicle;
	string getstring;

	inFile.open("Vehicle.txt");

	 while (getline(inFile, getstring))
	{
		// Line contains string of length > 0 then save it in vector
		if(getstring.size() > 0)
			heroVehicle.push_back(getstring);
	}

		srand ( time(NULL) ); //initialize the random seed

    int RandIndex = rand() % 121;
    cout << heroVehicle[RandIndex];

    //Testing for proper size and placement within vector
    //cout << heroVehicle.size() << endl;
    //cout << heroVehicle[0] << endl;
	inFile.close();
}

int main()
{
    //Declaration for loops to continue receiving a superhero
    string response;

    //While the answer is not No, continue to ask question
    while (response != "N") {
    cout << "Would you like to see a superhero name?(Y/N)" << endl;
    cin >> response;

    //If the answer is Yes, send superhero response
    if (response == "Y") {

    cout << "The ";
    heroAdj();
    cout << " ";
	hero1();
	cout << " ";
    hero2();
    cout << endl << "Transportation: ";
    hero1();
    cout << " ";
    heroVehicle();
    cout << endl << endl;
    }

    //If the answer is No, end program
    else if (response == "N") {
        cout << endl;
        cout << "Thanks for playing" << endl;
    }
    }

	return 0;
}
