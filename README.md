# README #

	I have chosen to tackle Option 2 for my C++ II final project. The title of the project will be the “Super-Hero Name-inator” as requested by the client Crazee Games. 
	The task at hand requires the creation of a randomly generated superhero with two names, an adjective, and a mode of transportation. 
	I have been provided with the proper text files which include the multiple names, adjectives, and vehicles. 
	
	Appropriate functions will be used to search the text files for random adjectives, name1, name2, and the transportation. 
	Each run through the program will access a different, random adjective, name, and transportation. 
	Therefore, there will have to be a way to randomly access each of these categories on each run of the program in order provide the wanted results. 
		
	I will also attempt to use Git and Bitbucket. This will help me as I progress through my own different steps and levels within the program. 
	As time continues I will be able to see the progress and possible mistakes or places for improvement. 